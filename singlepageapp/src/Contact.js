import React, { Component } from "react";
 
class Contact extends Component {
  render() {
    return (
      <div>
          <h2>Contact</h2>
        <p>Contact us pages are often the go-to for a new visitor on a mission. 
            It's where they go when they have a question and truly want to speak to an individual at your organization. 
            They exist to serve the user with the purpose of providing the user with information on how they can get in touch with you.
        </p>
      </div>
    );
  }
}
 
export default Contact;